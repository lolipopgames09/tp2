package ca.cegepdrummond;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mediaClick(View view) {

    }

    public void videosClick(View view) {
        Intent videoIntent;
        videoIntent = new Intent(this, Videos.class);
        startActivity(videoIntent);
    }

    public void photosClick(View view) {
        Intent photoIntent;
        photoIntent = new Intent(this, Photos.class);
        startActivity(photoIntent);
    }
}