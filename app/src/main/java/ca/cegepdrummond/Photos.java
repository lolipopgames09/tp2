package ca.cegepdrummond;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Photos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos);
    }
    public void mediaClick(View view) {
        Intent mediaIntent;
        mediaIntent = new Intent(this, MainActivity.class);
        startActivity(mediaIntent);
    }

    public void videosClick(View view) {
        Intent videoIntent;
        videoIntent = new Intent(this, Videos.class);
        startActivity(videoIntent);
    }

    public void photosClick(View view) {

    }

}
