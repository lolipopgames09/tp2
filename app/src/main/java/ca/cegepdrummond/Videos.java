package ca.cegepdrummond;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class Videos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videos);
        VideoView vidView = findViewById(R.id.videoView);
        vidView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.testclipvail);
        MediaController mCont = new MediaController(this);
        mCont.setAnchorView(vidView);
        vidView.setMediaController(mCont);
    }

    public void mediaClick(View view) {
        Intent mediaIntent;
        mediaIntent = new Intent(this, MainActivity.class);
        startActivity(mediaIntent);
    }

    public void videosClick(View view) {

    }

    public void photosClick(View view) {
        Intent photoIntent;
        photoIntent = new Intent(this, Photos.class);
        startActivity(photoIntent);
    }

}
